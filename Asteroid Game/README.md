# Pismeni ispit 09.02.2023.

## Changes 23/02/2023

- Smanjen kod u entities.py odnosno kod asteroids klase

- Popravljen bug kod pauziranja asteroida na način da je dodan kod u update funkciju kod level.py skripte

- Kod koji je dodan:
```
  keys = pygame.key.get_pressed()
        if keys[pygame.K_t] is False:
            for asteroid in self.asteroids:
                asteroid.update()
```
  Na ovaj način smo osigurali da se neče promijeniti direction jer smo blokirali pokretanje funkcije update ako je tipka t pritisnuta.

## Download
- Be in this directory
- go to the download button which is left of Clone button
- Choose whatever format you want to be downloaded from Download this directory

## NAPOMENE

### Pazite na `git config`, postavite svoje ime i email prije commitanja

- Svaki zadatak riješiti i spremiti u vlastiti commit, nakon
rješavanja ispita pushati sve na svoj repozitorij

- Ne miješati fileove različitih zadataka u isti commit. U commitu za pojedini
  zadatak trebaju biti samo oni fileovi koji direktno utječu na taj zadatak.

- U slučaju greške pri commitu, napraviti novi commit s ispravkom i nazvati ga
  "Fix za N. zadatak", gdje je N broj zadatka za koji se radi ispravak

- U commitovima ignorirati virtual environment i pycache fileove.


## Zadaci

### 1. Pokrenuti projekt (10 boda)

- Preuzeti promjene sa repozitorija i spojiti ih u svoj repozitorij
- Pokrenuti igru
- Promjeniti boju svih tekstova u igri u plavu

### 2. Healthpack rotation (10 boda)

- HealthPack se rotira na mjestu, svaki frame za jedan stupanj
- Pri stvaranju HealthPacka na ekranu, slučajno odabere smjer rotacije

### 3. Brzina metaka (10 boda)

- Promjeniti brzinu metaka koje ispali Turret na 4, a onih koje ispali igrač na
  8

### 4. Brzina asteroida (10 boda)

- Pri kreiranju asteroida potrebno im je postaviti slučajnu brzinu između 1 i 4


### 5. Kolizija turreta i asteroida (10 bodova)

- Kada dođe do kolizije između asteroida i turreta, oba trebaju biti uništena






