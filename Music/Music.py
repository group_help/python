import math

def main():
    names=[]
    lenght=[]
    cont=1
    f=open("songs.txt","w+")            #This means openning file in the same directory as is this this Music.py

    while cont:
        line=input("Enter song name and lenght: ")
        f.write(line+'\n')
        cont=int(input("do you wanna continue 1(continue) 0(stop): "))

    f.seek(0)

    for line in f:
        names.append(' '.join(line.split()[:len(line.split())-1:1]))            #names sprema imena pjesama na nacin da sve osim zadnjeg stringa u redu je ime

        data=line.split()[len(line.split())-1::1]                 #numbers sprema duljinu pjesme na nacid da je zadnji string duljina pjesme
        numbers=data[0].split(':')
        number=int(numbers[0])*60+int(numbers[1])

        lenght.append(number)

    indexMin=lenght.index(min(lenght))                 #Ovdje dohvaćamo index pjesme sa najmanjom duljinom na način da metodi lenght.index() kao parametar predajemo element koji ima najmanju vrijednost a to se dobiva sa min(naziv_array)
    indexMax=lenght.index(max(lenght))                 #Ovdje dohvaćamo index pjesme sa najvećom duljinom na način da metodi lenght.index() kao parametar predajemo element koji ima najveću vrijednost a to se dobiva sa max(naziv_array)
    med=sum(lenght)/len(lenght)
    indexMed=min(range(len(lenght)), key=lambda i: abs(lenght[i]-med))      #Dobivamo index pjesme sa srednjom duljinom na način da odredimo minimalno odstupanje duljine pjesme od srednje duljine pjesme

    print(f"Longest song: {names[indexMax]} duration: {math.floor(lenght[indexMax]/60)}:{lenght[indexMax]%60}\n")
    print(f"Average song: {names[indexMed]} duration: {math.floor(lenght[indexMed]/60)}:{lenght[indexMed]%60}\n")
    print(f"Shortest song: {names[indexMin]} duration: {math.floor(lenght[indexMin]/60)}:{lenght[indexMin]%60}\n")
    
    



if __name__=="__main__":
    main()