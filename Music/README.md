# Python



## Description

Python script that is used for storing and reading songs with their duration to .txt document

## Developer
Nikola Muženjak

## Usage
Start Music.py and then add song.
Examples:
```
Bohemian rapsody 4:33
Unravel 2:11
```
After writting one song you have to choose by pressing 1 or 0 if you want to continue. If you choose yes you add song again like in example above.
When you are finished with adding songs type 0 and program will print longest, shortest and closest to average lenght songs.
